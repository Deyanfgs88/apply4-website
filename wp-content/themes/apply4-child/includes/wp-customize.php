<?php include 'ysnp.php';
function ses_customize_register($wp_customize) {

//------------------------------------Sections
    $wp_customize->add_section('footer_section',array(
        'title'     => __('Footer',THEME_LONG_NAME),
        'priority'  => 30,
    ));
    $wp_customize->add_section('social_icons',array(
        'title'     => __('Social Networks & Email',THEME_LONG_NAME),
        'priority'  => 30,
   ));
//------------------------------------Settings
    //-------------------------------Address
        $wp_customize->add_setting('uk_address')->transport = 'postMessage';
        $wp_customize->add_setting('uk_phone')->transport = 'postMessage';
        $wp_customize->add_setting('us_address')->transport = 'postMessage';
        $wp_customize->add_setting('us_phone')->transport = 'postMessage';
    //--------------------------------Social Network
        $wp_customize->add_setting('twitter')->transport = 'postMessage';
        $wp_customize->add_setting('linkedin')->transport = 'postMessage';
        $wp_customize->add_setting('email')->transport = 'postMessage';
    //-------------------------------- Politics      
        $wp_customize->add_setting('cookies')->transport = 'postMessage';
        $wp_customize->add_setting('terms')->transport = 'postMessage';
        $wp_customize->add_setting('privacy')->transport = 'postMessage';
//------------------------------------Controls
    //-------------------------------Address
        $wp_customize->add_control(
            'uk_information_address',
            array(
                'label'     =>__('UK Address',THEME_LONG_NAME),
                'settings'  => 'uk_address',
                'priority'  => 10,
                'section'   => 'footer_section'
        ));
        $wp_customize->add_control(
            'uk_information_phone',
            array(
                'label'     =>__('UK Phone',THEME_LONG_NAME),
                'settings'  => 'uk_phone',
                'priority'  => 10,
                'section'   => 'footer_section'
        ));
        $wp_customize->add_control(
            'us_information_address',
            array(
                'label'     =>__('US Address',THEME_LONG_NAME),
                'settings'  => 'us_address',
                'priority'  => 10,
                'section'   => 'footer_section'
        ));
        $wp_customize->add_control(
            'us_information_phone',
            array(
                'label'     =>__('US Phone',THEME_LONG_NAME),
                'settings'  => 'us_phone',
                'priority'  => 10,
                'section'   => 'footer_section'
        ));
    //--------------------------------Social Network
        $wp_customize->add_control(
            'social_twitter',
        array(
            'label'     =>__('Twitter',THEME_LONG_NAME),
            'settings'  => 'twitter',
            'priority'  => 10,
            'section'   => 'social_icons'
        ));
        $wp_customize->add_control(
            'social_linkedin',
            array(
                'label'     =>__('Linkedin',THEME_LONG_NAME),
                'settings'  => 'linkedin',
                'priority'  => 10,
                'section'   => 'social_icons'
        ));
        $wp_customize->add_control(
            'social_email',
            array(
                'label'     =>__('Email',THEME_LONG_NAME),
                'settings'  => 'email',
                'priority'  => 10,
                'section'   => 'social_icons'
        ));

    //-------------------------------- Politics    

        $wp_customize->add_control(
            new WP_Customize_DropdownPages_Control(
                $wp_customize,
                    'cookies_control',
                    array(
                        'label'     =>__('Select Cookie Page',THEME_LONG_NAME),
                        'settings'  => 'cookies',
                        'priority'  => 10,
                        'section'   => 'footer_section',
                )
            )
        );

        $wp_customize->add_control(
            new WP_Customize_DropdownPages_Control(
                $wp_customize,
                'terms_control',
                array(
                    'label'     =>__('Select Terms & Conditions Page',THEME_LONG_NAME),
                    'settings'  => 'terms',
                    'priority'  => 10,
                    'section'   => 'footer_section',
                )
            )
        );
        $wp_customize->add_control(
            new WP_Customize_DropdownPages_Control(
                $wp_customize,
                'privacy_control',
                array(
                    'label'     =>__('Select Privacy Policy Page',THEME_LONG_NAME),
                    'settings'  => 'privacy',
                    'priority'  => 10,
                    'section'   => 'footer_section',
                )
            )
        );
}
add_action( 'customize_register', 'ses_customize_register' );

if (class_exists('WP_Customize_Control')) {
    class WP_Customize_DropdownPages_Control extends WP_Customize_Control {
         public function render_content() { ?>
        <h3><?php echo $this->label; ?></h3>
            <select  <?php $this->link(); ?>>
            <?php
                query_posts(array( 
                'post_type' => 'page',
                'post_status' => 'publish',
                'posts_per_page' => '20',
                'orderby' => 'date', 
                'order' => 'ASC'
            ) );  
                while (have_posts()) : the_post(); ?>
                <option value = "<?php echo get_the_ID(); ?>"><?php echo get_the_title();?></option>       
                <?php endwhile ?>
            </select>
       <?php  }
    }
}

