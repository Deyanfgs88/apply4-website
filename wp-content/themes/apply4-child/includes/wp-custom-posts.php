<?php include 'ysnp.php';

function custom_post_type() {
    $customFile = ['apps', 'team'];
    foreach($customFile as $CustomPostType){
        include_once  THEME_ROOT_PATH.'includes/custom_post_type/' . $CustomPostType . '.php';
    }
}

add_action( 'init', 'custom_post_type', 0 );