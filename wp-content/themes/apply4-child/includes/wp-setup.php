<?php include 'ysnp.php'; // this path needs to be added manually for each file

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  CONSTANTS
    define( 'THEME_SHORT_NAME'  , 'a4'                           );
    define( 'THEME_LONG_NAME'   , 'apply4'             );
    define( 'THEME_ROOT_PATH'   , get_stylesheet_directory() . '/'      );
    define( 'THEME_ROOT'        , get_stylesheet_directory_uri() .'/'   );
    define( 'PARENT_ROOT_PATH'  , get_template_directory() . '/'        );
    define( 'PARENT_ROOT'       , get_template_directory_uri() .'/'     );

    include_once PARENT_ROOT_PATH . 'includes/wp-setup.php';


    ses_include_files( THEME_ROOT_PATH . THEME_INCLUDES , [
        'wp-enqueue-scripts',
        'wp-custom-posts'
    ] , false );