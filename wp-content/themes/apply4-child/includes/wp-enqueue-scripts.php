<?php include_once 'ysnp.php';

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ENQUEUE CSS
    add_action( 'wp_enqueue_scripts' , function(){

        wp_register_style( // - - - - - - - - - - - - - - - - Theme Style
            'theme-style',
            THEME_CSS . 'theme-style.css',
            array( ),
            '16.11.01'
        );

        wp_enqueue_style( 'theme-style' );

    }, 20 ); 

  
    add_action ( 'login_enqueue_scripts' , function(){
        wp_register_style( // - - - - - - - - - - - - - - - - Login Style
            'admin-style',
            THEME_CSS . 'admin-style.css',
            array( ),
            '16.11.01'
        );

        wp_enqueue_style( 'admin-style' );

    }, 200);




// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ENQUEUE JS
    add_action( 'wp_enqueue_scripts' , function(){

        
        wp_register_script( // - - - - - - - - - - - - - - - D3
            THEME_SHORT_NAME . '-d3',
            THEME_JS .'d3.min.js',
            array( 'jquery' ),
            '17.07.25' 
        ); 

        wp_register_script( // - - - - - - - - - - - - - - - Customize
            THEME_SHORT_NAME . '-customize-control',
            THEME_JS .'customize-control.js',
            array('jquery', 'customize-preview'),
            '16.1'
        );

        wp_register_script( // - - - - - - - - - - - - - - - Customize
            THEME_SHORT_NAME . '-contactf7-action',
            THEME_JS .'contactf7-action.js',
            array('jquery'),
            '27.7'
        );

        wp_register_script( // - - - - - - - - - - - - - - - CircleAnimation
            THEME_SHORT_NAME . '-CircleAnimation',
            THEME_JS .'CircleAnimation.js',
            array( 'jquery' ,THEME_SHORT_NAME . '-d3'),
            '17.07.25' 
        );

        global $post;
        if( in_array( $post->post_name , [ 'contact-us' , 'request-a-demo' ] ) ):
            wp_enqueue_script( THEME_SHORT_NAME . '-contactf7-action' );
        elseif( in_array( $post->post_name ,[ 'homepage' ] ) ):
            wp_enqueue_script( THEME_SHORT_NAME . '-CircleAnimation' );
        endif;
        
        wp_enqueue_script( THEME_SHORT_NAME . '-customize-control' );
    }, 21 ); // END enqueue Javascript