<?php

/*

Template Name: Cookie Policy Page

*/

get_header(); 
the_post(); ?>

<div class="container-fluid">
   <div class="the-content">
        <?php //the_content(); ?>
        <div class="row">
            <div class="col-md-8">
                <?php
                    if(get_field('cookie_police_title')) {
                        echo '<h1>';
                        the_field('cookie_police_title');
                        echo '</h1>';
                    }
                    if(get_field('cookie_police_subtitle')) {
                        echo '<h2>';
                        the_field('cookie_police_subtitle');
                        echo '</h2>';
                    }
                    if(get_field('cookie_police_text')) {
                        echo '<div class="cookie-policy-text">';
                        the_field('cookie_police_text');
                        echo '</div>';
                    }
                ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>