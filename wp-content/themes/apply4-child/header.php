<?php include_once 'includes/ysnp.php'; // this path needs to be added manually for each file ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<?php wp_head( ); ?>
</head>
<body <?php body_class( ); ?> >
<div id="page-wrap">
	<header>
		<div class="container-header">
			
			<div class="header-top-mobile hidden-sm hidden-md hidden-lg">
				<div class="logo">
                    <a href="<?php echo get_home_url();?>">
                        <img src="<?php echo get_theme_mod( 'secundary_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
                    </a>                    
				</div>
				<div class="social-networks">
                    <?php
                        $link_social_twitter = get_theme_mod('twitter');
                        $link_social_linkedin = get_theme_mod('linkedin');
                        if (empty($link_social_twitter)){
                            $link_social_twitter = 'http://twitter.com';
                        }
                        if (empty($link_social_linkedin)){
                            $link_social_linkedin = 'http://linkedin.com';
                        }
                    ?>
					<a href="<?php echo $link_social_twitter; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					<a href="<?php echo $link_social_linkedin; ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
				</div>
            </div><!-- .header-top-mobile-->

            <div class="logo-white hidden-xs">
                <a href="<?php echo get_home_url();?>">
                    <img src="<?php echo get_theme_mod( 'secundary_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
                </a>
            </div> <?php // .logo-white ?>
            
            <div class="container-menu">

                <div class="collapse navbar-collapse container-custom-menu" id="mydropdown">
                     <?php if ( has_nav_menu( 'primary' ) ) : ?>
                        <div class="main-menu">
                            <?php
                                wp_nav_menu( array(
                                    'theme_location' => 'primary'
                                ) );
                            ?>
                        </div>
                    <?php endif; ?>
                </div> <!-- .navbar-collapse -->
                
                <!-- Menu Mobile -->
                <div class="navbar-header">
                        <button class="navbar-toggle collpased" data-toggle="collapse" data-target="#mydropdown">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>                    
                </div>
                <!-- .Menu Mobile -->

            </div> <!-- .container-menu -->


            <div class="social-networks-sm-md email hidden-xs">
                <div class="email-header hidden-xs">
                    <a href="mailto:info@apply4.com">info@apply4.com</a>
                </div> <?php // .email-header ?>
                <div class="social">
                    <a href="<?php echo $link_social_twitter; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    <a href="<?php echo $link_social_linkedin; ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                </div>
            </div>

        </div> <!-- .container-header -->
       
	</header>
	<div id="content-wrap">