<?php

/*

Template Name: About Page

*/

get_header(); 
the_post(); 

$team = get_field('about_team');
?>

<div class="container-fluid">
   <div class="the-content">
        <?php //the_content(); ?>
        <div class="default-content">
            <div class="row">
                <div class="col-md-7">
                    <?php
                        if(get_field('about_title')) {
                            echo '<h1>';
                            the_field('about_title');
                            echo '</h1>';
                        }
                        if(get_field('about_subtitle')) {
                            echo '<h2>';
                            the_field('about_subtitle');
                            echo '</h2>';
                        }
                        if(get_field('about_text')) {
                            echo '<div class="about-text">';
                            the_field('about_text');
                            echo '</div>';
                        }
                    ?>
                </div>
            </div>
            <div class="team">
                <?php foreach($team as $member): ?>
                    <div id="<?php echo $member->ID?>" class="team-member">
                        <a href="#">
                            <div class="image-container">
                                <?php echo wp_get_attachment_image( get_field('team_image',$member->ID),'medium',false, 'class=img-circle') ?>
                                <div class="overlay img-circle">
                                    <img src="<?php echo THEME_IMAGES; ?>arrow-right-apply4-white.png" alt="arrow right" title="arrow right">
                                </div>
                            </div>
                        </a>
                        <p class="team-member-role">
                            <?php echo the_field('team_role',$member->ID); ?>
                        </p>
                        <p class="team-member-name">
                            <?php echo the_field('team_name',$member->ID); ?>
                        </p>
                        <div class="social">
                            <ul>
                                <?php if( get_field('team_twitter',$member->ID) ): ?>
                                    <li><a href="<?php echo the_field('team_twitter',$member->ID); ?>"><i class="fa fa-twitter"></i></a></li>
                                <?php endif; ?>
                                <?php if( get_field('team_linkedin',$member->ID) ): ?>
                                    <li><a href="<?php echo the_field('team_linkedin',$member->ID); ?>"><i class="fa fa-linkedin"></i></a></li>
                                <?php endif; ?>
                                <?php if( get_field('team_email',$member->ID) ): ?>
                                    <li><a href="<?php echo the_field('team_email',$member->ID); ?>"><i class="fa fa-envelope-o"></i></a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        
                    </div>
                <?php endforeach;?>
            </div>
        </div>

        <?php foreach($team as $member): ?>
            <div id="fullInformation-<?php echo $member->ID?>"class="team-member solo">
                <a href="#" class="close-button-team img-circle" id="close-button-<?php echo $member->ID?>">
                    <i class="fa fa-times" aria-hidden="true"></i>    
                </a>

                <?php echo wp_get_attachment_image( get_field('team_image',$member->ID),'medium',false, 'class=img-circle') ?>

                <p class="team-member-role">
                    <?php echo the_field('team_role',$member->ID); ?>
                </p>
                <div class="social-container">
                    <p class="team-member-name">
                        <?php echo the_field('team_name',$member->ID); ?>
                    </p>
                    <div class="social">
                        <ul>
                            <?php if( get_field('team_twitter',$member->ID) ): ?>
                                <li><a href="<?php echo the_field('team_twitter',$member->ID); ?>"><i class="fa fa-twitter"></i></a></li>
                            <?php endif; ?>
                            <?php if( get_field('team_linkedin',$member->ID) ): ?>
                                <li><a href="<?php echo the_field('team_linkedin',$member->ID); ?>"><i class="fa fa-linkedin"></i></a></li>
                            <?php endif; ?>
                            <?php if( get_field('team_email',$member->ID) ): ?>
                                <li><a href="<?php echo the_field('team_email',$member->ID); ?>"><i class="fa fa-envelope-o"></i></a></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>

                <div class="team-member-description">
                    <?php echo the_field('team_description',$member->ID); ?>
                </div>
            </div>
        <?php endforeach;?>
    </div>
</div>

<?php get_footer(); ?>