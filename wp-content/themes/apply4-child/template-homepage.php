<?php

/*

Template Name: Homepage

*/
get_header(); 
//the_post(); 
$apps = get_field('hp_general_apps');
?>

<section class="section-first-part-homepage">
    <div class="container-fluid">
        <div class="the-content">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="container-image-video">
                        <img class="image-video" src="<?php the_field('image_video'); ?>" alt="image video" title="image video">
                        <div class="video">
                            <iframe src="https://player.vimeo.com/video/<?php the_field('id_video'); ?>?color=ffffff&title=0&byline=0&portrait=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                        </div>
                        <div class="cta-play-video">
                            <img src="<?php echo THEME_IMAGES; ?>arrow-right-apply4-white.png" alt="arrow right" title="arrow right">
                            <p>play video</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="container-title-sub-desc-homepage">
                        <div class="title-hp">
                            <h1><?php the_field('hp_general_title'); ?></h1>
                        </div>
                        <div class="subtitle-hp">
                            <h4><?php the_field('hp_general_subtitle'); ?></h4>
                        </div>
                        <div class="description-hp">
                            <?php the_field('hp_general_description'); ?>
                        </div>
                    </div> <?php // .container-title-sub-desc-homepage ?>

                    <div class="container-apps-homepage">
                        <?php foreach($apps as $app): ?>
                            <div class="app-logoBox">
                                <a href="<?php echo get_permalink($app->ID)?>" class="app-logo <?php the_field('app_name',$app->ID) ?>">
                                    <?php echo wp_get_attachment_image( get_field('app_logo',$app->ID),'medium',false) ?>
                                </a>
                            </div>
                        <?php endforeach;?>
                    </div>  <?php // .container-apps-homepage ?>

                </div>
            </div>
        </div> <?php // .the-content ?>
    </div> <?php // .container-fluid ?>

</section> <? // .section-first-part-homepage ?>

<section class="key-aspects">
    <div class="container-fluid">
        <div class="the-content">
            <div class="row">
                <div class="col-md-4">
                    <div class="key-1">
                        <h2><?php the_field('title_key_1') ?></h2>
                        <?php the_field('text_key_1') ?>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="key-2">
                        <h2><?php the_field('title_key_2') ?></h2>
                        <?php the_field('text_key_2') ?>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="key-3">
                        <h2><?php the_field('title_key_3') ?></h2>
                        <?php the_field('text_key_3') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php global $images, $client_title;
    $client_title = 'Client portfolio';
    $uk_clients = get_field('uk_clients');
    $usa_clients = get_field('usa_clients');
    get_template_part('templates/partials/clients_section');
?>


<?php get_footer(); ?>

