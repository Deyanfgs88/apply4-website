<?php

/*

Template Name: Contact Page

*/ 

get_header(); 
the_post();
?>
    <?php $link_twitter= get_theme_mod('twitter');?>
    <?php $link_email= get_theme_mod('email');?>
    <?php $link_linkedin= get_theme_mod('linkedin');?>


    <div class="container-fluid">
        <div class="the-content">
            
            <div class="row">
                <div id="thank-message" class="oculto">
                    <h2>Thank you for getting in touch</h2>
                    <p>We appreciate you contacting Apply4 and will get back to you very soon about your enquiry.</p>
                </div>

                <div id="pageContact">
                    <div class="col-sm-12 col-md-7 col-lg-7 ">
                        <h1 class="form-title">
                            <?php  if (get_field('title-form')) echo get_field('title-form'); ?>
                        </h1>
                    </div>

                    <div class="col-sm-12 col-md-7 col-lg-7 ">
                       
                        <?php $contactForm = get_field('contact_form'); ?>
                        <div class="ss-contact-form">
                            
                            <h2 class="form-title">
                                <?php  if ( get_field('subtitle-form') ) echo get_field('subtitle-form'); ?>
                            </h2>
                            <p class="form-title">
                                <?php  if ( get_field('subtitle-form') ) echo get_field('description_contact_form'); ?> </p>

                            <div class="widget-contact-text  hidden-md hidden-lg in-row">
                                <?php dynamic_sidebar( 'sidebar' ); ?>

                            </div>
                            <div class="social-icons hidden-md hidden-lg ">
                                <?php if ($link_twitter != ''):?>
                                <a href="<?php echo get_theme_mod('twitter'); ?>"><i class="fa fa-twitter fa-2x"></i></a>
                                <?php endif; ?>
                                <?php if ($link_linkedin != ''):?>
                                <a href="<?php echo get_theme_mod('linkedin'); ?>"><i class="fa fa-linkedin fa-2x"></i></a>
                                <?php endif; ?>
                                <?php if ($link_email != ''):?>
                                <a href="<?php echo get_theme_mod('email'); ?>"><i class="fa fa-envelope fa-2x"></i></a>
                                <?php endif; ?>
                            </div>
                        </div>

                        <?php  // echo do_shortcode("[contact-form-7 id='$contactForm->ID' title='Contact us']"); ?>
                        <?php get_template_part('templates/partials/contact-request-demo/zoho-contact-form'); ?>

                    </div>
                    <div class="col-sm-12 col-md-offset-1 col-md-4 hidden-xs hidden-sm">
                        <div class="widget-contact-text ">
                            <?php dynamic_sidebar( 'sidebar' ); ?>
                        </div>
                        <div class="social-icons">
                            <?php if ($link_twitter != ''):?>
                            <a href="<?php echo get_theme_mod('twitter'); ?>" target="_blank"><i class="fa fa-twitter fa-2x"></i></a>
                            <?php endif; ?>
                            <?php if ($link_linkedin != ''):?>
                            <a href="<?php echo get_theme_mod('linkedin'); ?>" target="_blank"><i class="fa fa-linkedin fa-2x"></i></a>
                            <?php endif; ?>
                            <?php if ($link_email != ''):?>
                            <a href="mailto:<?php echo get_theme_mod('email'); ?>"><i class="fa fa-envelope fa-2x"></i></a>
                            <?php endif; ?>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <?php get_footer(); ?>