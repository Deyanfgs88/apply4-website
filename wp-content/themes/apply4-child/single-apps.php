<?php

get_header(); 
the_post(); ?>

<section class="hero-image">
    <div class="image-box" style="background-image: url('<?php echo get_field('hero_image'); ?>');">
    </div>
</section>

<section class="container-fluid">
   <div class="the-content">
       <?php //the_content(); ?>
       <div class="app-logo">
            <?php echo wp_get_attachment_image( get_field('app_logo'),'medium',false) ?>
       </div>
       <div class="app-wrap  <?php echo'color-'.get_field('featured_color') ?>">
            <div class=" appContent-col">
                <div class="info-wrap">
                    <h2> <?php the_field('app_main_title') ?> </h2>
                    <div class="description">
                        <?php the_field('app_main_content') ?>
                    </div>
                </div>
            </div>
            <div class=" benefitsFeatures-col">

                <div class="feature-container">
                    <div class="feature-container-items row">
                        
                    <?php $features = get_field('items');
                        foreach( $features as $feature ):
                    ?>
                        <div class="feature-item col-xs-12 col-sm-6 col-md-12">
                            <h3 class=" <?php echo'color-'.get_field('featured_color') ?> "> <?php echo $feature['feature_title'] ?> </h3>
                            <ul class="feature-content">
                                <?php 
                                $items = explode("\r\n",$feature['feature_items']);
                                foreach( $items as $item):?>
                                <?php if (empty($item)) continue; ?>
                                    <li> <?php echo  $item?> </li>
                                <?php endforeach;?>
                            </ul>
                        </div>

                    <?php endforeach;?>
                    </div>
                    <div class="cta-request-a-demo">
                        <a href="<?php echo get_field('url_request_a_demo'); ?>" class="request_btn apply_btn"> 
                            Request a demo
                        </a>
                    </div>
                </div>
                

            </div>
       </div>

    </div>


</section>
<?php global $featured_color,$app_name,$quote;
    $featured_color= get_field('featured_color');
    $app_name = get_field('app_name');
    $quote = get_field('quote');
    get_template_part('/templates/partials/quote_section');
 ?>

<?php get_footer(); ?>

