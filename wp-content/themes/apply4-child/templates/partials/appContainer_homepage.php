<?php global $app ?>

<div class="app-item <?php echo $app['name']?> ">
    <div class="container-fluid app_content">
        <div class="the-content">
            <h2 class="app_name"> <?php echo $app['content']['title'] ?> </h2>
            <div class="app_wrap">
                <?php if (!isset($app['content']['href'])): ?>
                    <div class="app-logo">
                        <?php echo wp_get_attachment_image( $app['content']['logo_id'],'medium',false) ?>
                    </div>
                <?php else: ?>
                    <a href="<?php echo $app['content']['href']?>" class="app-logo">
                        <?php echo wp_get_attachment_image( $app['content']['logo_id'],'medium',false) ?>
                    </a>
                <?php endif;?>
                <div class="app-description">
                    <h4> <?php echo $app['content']['subtitle'] ?> </h4>
                    <div class="description">
                        <?php echo $app['content']['description'] ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="app_feature_image  ">
        <div id="app-imageMain-<?php echo $app['pos']?>" class="image-box thumbnail">
            <?php echo get_the_post_thumbnail($app['id'],'full',['class' => 'img-responsive img-circle']) ?>
        </div>
    </div>
</div>