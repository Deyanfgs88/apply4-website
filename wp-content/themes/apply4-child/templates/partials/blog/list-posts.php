<?php

$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

$query_args = array( 
    'post_type'     => 'post', 
    'paged'         => $paged,
    'page'          => $paged,
    'cat'           => '' 
);

if (get_queried_object_id() != 259) {
    $query_args['cat'] = get_queried_object_id();
}
global $a4_blog_query;
$a4_blog_query = new WP_Query( $query_args );
$posts = $a4_blog_query->posts;
?>

<div class="container-blog-post">
<?php
    foreach($posts as $post) {
        
?>
<div <?php post_class(); ?>>
    <a href="<?php echo get_permalink( $post->ID ) ?>">
        <div class="thumbnail">
            <?php
            if ( has_post_thumbnail() ) {
                ?>
                <?php the_post_thumbnail('post-theme-thumb'); ?>
                <div class="overlay">
                    <img src="<?php echo THEME_IMAGES; ?>arrow-right-apply4-white.png" alt="arrow right" title="arrow right">
                </div>
            <?php
            }else{ ?>
                <img src="<?php echo THEME_IMAGES; ?>blog_generic_thumb.png" alt="default post thumbnail" title="default thumbnail">
                <div class="overlay">
                    <img src="<?php echo THEME_IMAGES; ?>arrow-right-apply4-white.png" alt="arrow right" title="arrow right">
                </div>
            <?php 
            } 
            ?>
        </div> <!-- .thumbnail -->
    </a>

    <?php
        $cats = get_the_category($post->ID);
        $cats_name = $cats[0]->name;
        $grad_color = '';
        switch ($cats_name) {
            case 'trafficapp':
                $grad_color = 'red';
                break;
            case 'eventapp':
                $grad_color = 'green';
                break;
            case 'filmapp':
                $grad_color = 'blue';
                break;
            case 'marketsapp':
                $grad_color = 'orange';
                break;
            case 'venueapp':
                $grad_color = 'purple';
                break;
            default:
                $grad_color = 'apply4';
                break;
        }
    ?>

    <div class="category gradient-<?php echo $grad_color; ?>">
        <div class="icon-circle-apply4">
            <img src="<?php echo THEME_IMAGES; ?>circle-apply4-white.png" alt="circle apply4" title="circle apply4 logo">
        </div> <!-- .icon-circle-apply4 -->
        <div class="category-name">
            <p><?php echo $cats_name; ?></p>
        </div>
    </div> <!-- category -->
    <div class="date-content-post">
        <h4 class="date"><?php echo date("jS F Y", strtotime($post->post_date));//the_date('jS F Y'); ?></h4>
        <?php
            $title_post = get_the_title();
        ?>
        <p class="content-post"><?php echo $title_post; ?></p>
    </div>

</div><!-- .post -->
<?php
    } // endforeach
?>
</div><!-- .container-blog-post -->