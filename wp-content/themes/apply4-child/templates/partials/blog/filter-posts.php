<div class="filter-posts">
    <div id="myNavCat" class="collapse navbar-collapse in">
        <ul class="nav navbar-nav">
            <img class="arrow-down" src="<?php echo THEME_IMAGES; ?>apply4-arrow-down-blue.png" alt="arrow down" title="arrow down">
            <img class="arrow-up" src="<?php echo THEME_IMAGES; ?>apply4-arrow-up-blue.png" alt="arrow up" title="arrow up">
            <li class="cat-item cat-item-slide-toggle <?php if (get_queried_object_id() == 259) echo 'current-cat' ?>">all</li>
            <ul class="list-categories">
                <li class="cat-item cat-item-all <?php if (get_queried_object_id() == 259) echo 'current-cat' ?>"><a href="<?php echo get_home_url(); ?>/blog">all</a></li>
                <?php 
                    $args = array(
                        'orderby'            => 'name',
                        'order'              => 'ASC',
                        'show_count'         => 0,
                        'use_desc_for_title' => 0,
                        'title_li'           => 0,                        
                    );
                    wp_list_categories($args);
                ?>
            </ul>
        </ul>
    </div>
</div> <?php // .filter-posts ?>