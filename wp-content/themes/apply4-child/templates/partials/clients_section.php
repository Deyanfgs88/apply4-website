<?php global $client_title, $uk_clients, $usa_clients ?>

<section class="client-portfolio">
    <div class="container-fluid">
        <div class="the-content">
            <h2>
                <?php echo $client_title ?>
            </h2>
            <div class="clients">
                <div class="uk-clients">
                    <h3>UK Clients</h3>
                    <div class="list-uk">
                        <?php 
                        foreach ($uk_clients as $uk_client){
                            if (!empty($uk_client['url'])){ ?>
                                <a href="<?php echo $uk_client['url']; ?>" target="_blank"><?php echo $uk_client['name']; ?></a>
                            <?php } else{ ?>
                                <p><?php echo $uk_client['name']; ?></p>
                            <?php }
                        } ?>
                    </div>
                </div> <?php // .uk-clients ?>
                <div class="usa-clients">
                <h3>USA Clients</h3>
                <div class="list-usa">
                    <?php 
                        foreach ($usa_clients as $usa_client){
                            if (!empty($usa_client['url'])){ ?>
                                <a href="<?php echo $usa_client['url']; ?>" target="_blank"><?php echo $usa_client['name']; ?></a>
                            <?php } else{ ?>
                                <p><?php echo $usa_client['name']; ?></p>
                            <?php }
                        } ?>
                </div>
                </div> <?php // .usa-clients ?>
            </div>

        </div> <?php // .the-content ?>
    </div> <?php // .container-fluid ?>
</section>