<?php global $featured_color,$app_name,$quote?>
<section class="quote_section <?php echo 'gradient-'.$featured_color ?>">
    <div class="quote-wrap">
        <div class="app-quote <?php if (isset($app_name) ) echo $app_name;  ?>">
            <?php echo $quote  ?>
            <div class="author-quote">
                <p><?php the_field('company_quote') ?></p>
                <div class="forename-surname">
                    <p><?php the_field('forename_quote'); ?></p>
                    <p><?php the_field('surname_quote'); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>