<?php

get_header(); 
the_post(); ?>

<div class="container-fluid">
   <div class="the-content">
       <?php the_content(); ?>
    </div>
</div>

<?php get_footer(); ?>
