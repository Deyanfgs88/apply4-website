<?php
    include_once 'includes/wp-setup.php';
    include_once 'includes/wp-customize.php';


/*** Custom Login URL ***/
function custom_loginlogo_url($url) {
	return get_bloginfo('url');
}
add_filter( 'login_headerurl', 'custom_loginlogo_url' );

function register_widget_sidebar(){    
    register_sidebar( array(
        'name'          => 'Sidebar',
        'id'            => 'sidebar',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
        ) ); }   
add_action( 'widgets_init', 'register_widget_sidebar');


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - HOOK: AFTER SETUP THEME
    add_action( 'after_setup_theme' , function(){
    // add_image_size       => Documentation: https://developer.wordpress.org/reference/functions/add_image_size/
        global $image_sizes;
        $image_sizes = array(
            'post-theme-thumb' => array( 'width' => 727 , 'height' => 657 , 'crop' => true ), // this is an example
            'single-post-theme-thumb' => array( 'width' => 515 , 'height' => 323 , 'crop' => true ) // this is an example
        );

        foreach( $image_sizes as $size_id => $image_size ):
            add_image_size( 
                $size_id,
                $image_size[ 'width' ],
                $image_size[ 'height' ],
                $image_size[ 'crop' ]
            );
        endforeach;

    } ); // END after_setup_theme
