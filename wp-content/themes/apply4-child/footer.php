<?php include_once 'includes/ysnp.php'; // this path needs to be added manually for each file ?>
	</div> <?php // END content-wrap ?>
    <footer>
        <div class="container-fluid">
            <div class="the-content">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-7">
                        <div class="row">
                            <div class="col-xs-12 col-md-4 col-lg-3">
                                <div class="footer-header">
                                    <div class="logo">
                                        <a href="<?php echo get_home_url();?>">
                                            <img src="<?php echo get_theme_mod( 'secundary_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
                                        </a>
                                    </div>
                                    <span class="email_wrap "><a href="mailto:<?php echo get_theme_mod('email'); ?>"><?php echo get_theme_mod('email'); ?></a></span>

                                </div>

                            </div>
                            <div class="col-xs-12 col-md-8 col-lg-7">
                                <div class="social-wrap">
                                    <div class="social-links">
                                        <?php 
                                            $link_social_twitter = get_theme_mod('twitter');
                                            $link_social_linkedin = get_theme_mod('linkedin');
                                            if (empty($link_social_twitter)){
                                                $link_social_twitter = 'http://twitter.com';
                                            }
                                            if (empty($link_social_linkedin)){
                                                $link_social_linkedin = 'http://linkedin.com';
                                            }
                                        ?>
                                        <a href="<?php echo $link_social_twitter; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                        <a href="<?php echo $link_social_linkedin; ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="container-politics">
                                        <span class="politic"><a href=<?php echo get_permalink(get_theme_mod('cookies'));?> >Cookie Policy</a></span>
                                        <span class="politic"><a href=<?php echo get_permalink(get_theme_mod('terms'));?> >Terms & Conditions</a></span>
                                        <span class="politic"><a href=<?php echo get_permalink(get_theme_mod('privacy'));?> >Privacy Policy</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="copyright desktop">
                            <span>&copy; Apply4 Technology Ltd</span>
                            <span class="yellobelly">Made by Yellobelly</span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-5">
                        <div class="row">
                            <div class="footer_information">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="p-1 uk-info">
                                        <p class="information">UK contact information</p>
                                        <p class="address"><?php echo get_theme_mod('uk_address');?></p>
                                        <a href="tel:+<?php echo get_theme_mod('uk_phone'); ?>" class="phone"><?php echo get_theme_mod('uk_phone'); ?></a>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="p-1 usa-info">
                                        <p class="information">USA contact information</p>
                                        <p class="address"><?php echo get_theme_mod('us_address');?></p>
                                        <a href="tel:+18664197968" class="phone"><?php echo get_theme_mod('us_phone'); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="copyright tablet-mobile">
                    <span>&copy; Apply4 Technology Ltd</span>
                    <span class="yellobelly">Made by Yellobelly</span>
                </div>
            </div>
            
        </div>
    </footer>
</div> <?php // END page-wrap ?>
<?php wp_footer(); ?> 
</body>
</html>