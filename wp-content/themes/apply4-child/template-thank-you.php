<?php

/*

Template Name: Thank You Page

*/

get_header(); ?>

<div class="container-fluid thank-you">
    <a href="<?php echo get_home_url(); ?>"><div class="logo-apply4"></div></a>
    <div class="the-content">
        <h2>Thank you for getting in touch</h2>
        <p>We appreciate you contacting Apply4 and will get back to you very soon about your enquiry.</p>
    </div>
</div>

<?php get_footer(); ?>