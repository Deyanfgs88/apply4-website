<?php

get_header(); 
the_post(); ?>

<div class="container-fluid">
    <div class="the-content">
        <?php
            $post_id = $post->ID; // current post ID
            $cat = get_the_category();
            $current_cat_id = $cat[0]->cat_ID; // current category ID
            $current_cat_name = $cat[0]->name; // current category name
        ?>
        <div class="container-close-button-post">
            <a href="<?php echo get_home_url(); ?>/blog" class="close-button-single-post img-circle"><i class="fa fa-times" aria-hidden="true"></i></a>
        </div>
        <div class="logo-app-single-post">
            <?php 
                switch ($current_cat_name) {
                    case 'trafficapp':
                        ?>
                            <img src="<?php echo THEME_IMAGES; ?>Apply4_Trafficapp.png" alt="logo apply4 trafficapp" title="logo apply4 trafficapp">
                        <?php
                        break;
                    case 'eventapp':
                        ?>
                            <img src="<?php echo THEME_IMAGES; ?>Apply4_Eventapp.png" alt="logo apply4 eventapp" title="logo apply4 eventapp">
                        <?php
                        break;
                    case 'filmapp':
                        ?>
                            <img src="<?php echo THEME_IMAGES; ?>Apply4_Filmapp.png" alt="logo apply4 filmapp" title="logo apply4 filmapp">
                        <?php
                        break;
                    case 'marketsapp':
                        ?>
                            <img src="<?php echo THEME_IMAGES; ?>Apply4_Marketsapp.png" alt="logo apply4 marketsapp" title="logo apply4 marketsapp">
                        <?php
                        break;
                    case 'venueapp':
                        ?>
                            <img src="<?php echo THEME_IMAGES; ?>Apply4_Venueapp.png" alt="logo apply4 venueapp" title="logo apply4 venueapp">
                        <?php
                        break;
                    default:
                        break;
                }
            ?>            
        </div>
        <div class="container-date-title-thumbnail">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="date-title-post">
                        <?php the_date('jS F Y','<div class="post-date"><p>','</p></div>'); ?>
                        <h1>
                            <?php the_field('post_title'); ?>
                        </h1>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="post-thumbnail">
                        <?php
                            if ( has_post_thumbnail() ) {
                                the_post_thumbnail('single-post-theme-thumb');
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="post-content">
            <?php 
                if(get_field('post_content')) {
                    the_field('post_content');
                }
            ?>
        </div>
        <div class="container-prev-next-post">
            <?php
                $args = array( 
                    'category' => $current_cat_id,
                    'orderby'  => 'post_date',
                    'order'    => 'DESC'
                );
                $posts = get_posts( $args );
                // get IDs of posts retrieved from get_posts
                $ids = array();
                foreach ( $posts as $thepost ) {
                    $ids[] = $thepost->ID;
                }

                // get and echo previous and next post in the same category
                $thisindex = array_search( $post_id, $ids );
                $previd    = isset( $ids[ $thisindex - 1 ] ) ? $ids[ $thisindex - 1 ] : 0;
                $nextid    = isset( $ids[ $thisindex + 1 ] ) ? $ids[ $thisindex + 1 ] : 0;
                
                if ( $previd ) {
                    ?>                    
                    <a class="previous-link-post" rel="prev" href="<?php echo get_permalink($previd) ?>"><img class="arrow-left" src="<?php echo THEME_IMAGES; ?>apply4-arrow-left-blue.png" alt="arrow left" title="arrow left">view previous <?php echo $current_cat_name; ?> blog</a>
                    <?php
                }
                if ( $nextid ) {
                    ?>
                    <a class="next-link-post" rel="next" href="<?php echo get_permalink($nextid) ?>"><img class="arrow-right" src="<?php echo THEME_IMAGES; ?>apply4-arrow-right-blue.png" alt="arrow right" title="arrow right">                                        
                    view next <?php echo $current_cat_name; ?> blog</a>
                    <?php
                }
            ?>
        </div>
    </div>
</div>

<?php global $featured_color,$quote;

    switch ($current_cat_name) {
        case 'trafficapp':
            $featured_color = 'red';
            break;
        case 'eventapp':
            $featured_color = 'green';
            break;
        case 'filmapp':
            $featured_color = 'blue';
            break;
        case 'marketsapp':
            $featured_color = 'orange';
            break;
        case 'venueapp':
            $featured_color = 'purple';
            break;
        default:
            $featured_color = 'apply4';
            break;
    }
    $quote = get_field('post_quote');
    get_template_part('/templates/partials/quote_section');
 ?>

<?php get_footer(); ?>