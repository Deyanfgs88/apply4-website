<?php

get_header(); 
the_post(); ?>

<div class="container-fluid">
    <div class="the-content">
        <div class="container-title-blog-filter-posts">
            <div class="title-blog">
                <h2>blog</h2>
            </div>
            <div class="container-template-filter-posts">
                <?php get_template_part('templates/partials/blog/filter-posts'); ?>
            </div>
        </div><!-- .container-title-blog-filter-posts -->

        <?php get_template_part('templates/partials/blog/list-posts'); ?>

    </div>
</div>

<?php get_footer(); ?>