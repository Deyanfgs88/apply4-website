    (function() {
        /* * * * * * * * * * * * * * * * Variables */
        var circleAnimationTime = 2000;
        var fadeOutTime = 2500;
        var fadeInTime = 4000;

        /* * * * * * * * * * * * * * * * Functions */
        function slideAnimationApp(container, currentPos) {
            var currentElement = container[currentPos];
            jQuery(currentElement).fadeOut(fadeOutTime, function() {
                var nextPos = (currentPos + 1) % parseInt(container.length);
                var $nextElement = container[nextPos];

                jQuery($nextElement).fadeIn(fadeInTime, function() {
                    slideAnimationApp(container, nextPos);
                });
            });

        }

        /* * * * * * * * * * * * * * * * Events  */

        jQuery(function($) {
            $(".team > .team-member").click(function() {
                $(".default-content").hide();
                var id = "#fullInformation-" + this.id;
                $(id).show();
            });

            $(".close-button-team").click(function() {
                var id = "#fullInformation-" + this.id.slice(13, this.id.length);
                $(".default-content").show();
                $(id).hide();
            });
            var appSlide = $('.app_container .app-item');
            if (appSlide.length > 0) {
                new CircleAnimation('#app-imageMain-0', 'hidden', true, circleAnimationTime);

                for (i = 1; i < appSlide.length; i++) {
                    new CircleAnimation('#app-imageMain-' + i, 'visible', false);

                }

                setTimeout(function() {
                    slideAnimationApp(appSlide, 0)
                }, circleAnimationTime);
            }

            // add video when click cta in homepage

            $('.container-image-video .video').hide();

            $('.container-image-video .cta-play-video').click(function(){
                $('.container-image-video .video').show();
            });

            // toogle list categories filter

            $('#myNavCat ul.list-categories').hide();
            $(".navbar-nav .arrow-up").hide();

            $('#myNavCat li.cat-item-slide-toggle').click(function() {
                $('.navbar-nav').find('.list-categories').slideToggle();
                $('.navbar-nav').find('.arrow-down, .arrow-up').toggle();

            });

            var current_cat = $('.list-categories .current-cat').text();
            $('#myNavCat li.cat-item-slide-toggle').html(current_cat);

            
        });

    }).call(this);