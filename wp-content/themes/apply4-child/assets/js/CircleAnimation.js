/*
 * Circle animation object - appends svg circles with mask and animated if requested
 * Dependency: d3 version 4
 * Assumes a square div wrapper with a circular image (via border radius) to which the animation is appended
 *
 * @param imageWrapperId string        id element to which the circle animation is appended 
 * @param initialState string          hidden (no circles and prepared to animate) visible (circles shown and no animation) 
 * @param animate boolean              true or false to animate
 * @param ianimationSpeed integer      number of seconds to carry out the animation
 * @return void     appends the svg circles and animates if requested
 *
 *
 * Example (hidden and animated)  new CircleAnimation('#hero_image','hidden', true, 3);
 * Example (visible on creation)	 new CircleAnimation('#hero_image','visible', false);
 */

function CircleAnimation(imageWrapperId, initialState, animate, animationSpeed) {
    if (imageWrapperId.charAt(0) === '#') {
        this.imageId = imageWrapperId.substr(1);
    } else {
        this.imageId = imageWrapperId;
    }

    this.imageWrapperId = imageWrapperId;
    this.initialState = initialState;
    this.animateState = animate;
    this.animationSpeed = !animationSpeed ? 2000 : animationSpeed;

    this.smallCirclePositions = {
        hidden: {
            offset: '31.5%',
            radius: '24%',
            radius2: '23.5%'
        },
        visible: {
            offset: '25.8%',
            radius: '15.75%',
        },
    }

    this.largeCirclePositions = {
            hidden: {
                offset: '44%',
                radius: '42%',
                radius2: '41%'
            },
            visible: {
                offset: '36.8%',
                radius: '31%',
            },
        }
        /**
            this.smallCirclePositions = {
                hidden: {
                    offset: '31.5%',
                    radius: '24%',
                    radius2: '23.5%'
                },
                visible: {
                    offset: '25.5%',
                    radius: '15.75%',
                },
            }

            this.largeCirclePositions = {
                hidden: {
                    offset: '44%',
                    radius: '42%',
                    radius2: '41%'
                },
                visible: {
                    offset: '36.5%',
                    radius: '31%',
                },
            }
        */

    this.appendCircularAnimation = function() {
        var svgroot = d3.select(this.imageWrapperId)
            .append("svg");

        var maskSmall = svgroot
            .append("defs")
            .append("mask")
            .attr("id", "maskSmall-" + this.imageId);

        maskSmall.append("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", '100%')
            .attr("height", '100%')
            .style("fill", "white")
            .style("opacity", 1);

        maskSmall.append("circle")
            .attr("id", 'smallCircleMask-' + this.imageId)
            .attr("cx", this.smallCirclePositions[this.initialState].offset)
            .attr("cy", this.smallCirclePositions[this.initialState].offset)
            .attr("r", this.smallCirclePositions[this.initialState].radius);

        var maskLarge = svgroot
            .append("defs")
            .append("mask")
            .attr("id", "maskLarge-" + this.imageId);

        maskLarge.append("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", '100%')
            .attr("height", '100%')
            .style("fill", "white")
            .style("opacity", 1);

        maskLarge.append("circle")
            .attr("id", 'largeCircleMask-' + this.imageId)
            .attr("cx", this.largeCirclePositions[this.initialState].offset)
            .attr("cy", this.largeCirclePositions[this.initialState].offset)
            .attr("r", this.largeCirclePositions[this.initialState].radius);

        var svg = svgroot
            .attr("class", "series")
            .attr("width", '100%')
            .attr("height", '100%')
            .append("g")

        var circle1 = svg
            .append("circle")
            .attr("cx", this.smallCirclePositions.hidden.offset)
            .attr("cy", this.smallCirclePositions.hidden.offset)
            .attr("r", this.smallCirclePositions.hidden.radius2)
            .attr("mask", "url(#maskSmall-" + this.imageId + ")")
            .style("fill", "white")

        var circle2 = svg
            .append("circle")
            .attr("cx", this.largeCirclePositions.hidden.offset)
            .attr("cy", this.largeCirclePositions.hidden.offset)
            .attr("r", this.largeCirclePositions.hidden.radius2)
            .attr("mask", "url(#maskLarge-" + this.imageId + ")")
            .style("fill", "white")
    }

    this.animate = function() {
        var svgroot = d3.select(this.imageWrapperId);

        svgroot.select("#smallCircleMask-" + this.imageId)
            .transition()
            .duration(this.animationSpeed)
            .attr("cx", this.smallCirclePositions.visible.offset)
            .attr("cy", this.smallCirclePositions.visible.offset)
            .attr("r", this.smallCirclePositions.visible.radius)

        svgroot.select("#largeCircleMask-" + this.imageId)
            .transition()
            .duration(this.animationSpeed)
            .attr("cx", this.largeCirclePositions.visible.offset)
            .attr("cy", this.largeCirclePositions.visible.offset)
            .attr("r", this.largeCirclePositions.visible.radius);
    }


    this.appendCircularAnimation();
    if (this.animateState) this.animate();
}