(function($) {
    wp.customize('uk_address', function(value) {
        value.bind(function(to) {
            $('.uk-address').html(to);
        });
    });
    wp.customize('uk_phone', function(value) {
        value.bind(function(to) {
            $('.uk-phone').html(to);
        });
    });
    wp.customize('us_address', function(value) {
        value.bind(function(to) {
            $('.us-address').html(to);
        });
    });
    wp.customize('us_phone', function(value) {
        value.bind(function(to) {
            $('.us-phone').html(to);
        });
    });

})(jQuery);