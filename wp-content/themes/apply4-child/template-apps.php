<?php
/*

Template Name: Apps Page

*/
get_header(); 
the_post(); 

$apps = get_field('apps_order');
?>

<div class="container-fluid">
   <div class="the-content">
       <div class="pageContent-wrap row">
            <div class="col-sm-12 col-md-7 appsContent-col description-col">
                <div class="appPage-title">
                    <h1> <?php the_field('appPage_title') ?> </h1>
                </div>
                <h2> <?php the_field('appPage_hero_text') ?> </h2>
                <div class="description">
                    <?php the_field('appPage_description') ?>
                </div>
            </div>
            <div class="col-sm-12 col-md-5">
                <div class="container-benefits">

                    <div class="feature-container">
                        <div class="feature-container-items row">
                            
                            <?php $benefits = get_field('appsPage_benefits');
                                foreach( $benefits as $benefit ):
                            ?>
                                <div class="feature-item col-xs-12 col-sm-12 col-md-12">
                                    <h3><?php echo $benefit['benefits_appPage_title'] ?></h3>
                                    <ul class="feature-content">
                                        <?php 
                                        $items_appsPage_benefits = explode("\r\n", $benefit['benefits_appPage_items']);
                                        foreach( $items_appsPage_benefits as $item_appsPage_benefits):?>
                                        <?php if (empty($item_appsPage_benefits)) continue; ?>
                                            <li> <?php echo  $item_appsPage_benefits?> </li>
                                        <?php endforeach;?>
                                    </ul>
                                </div>

                            <?php endforeach;?>
                        </div>
                        <div class="cta-request-a-demo">
                            <a href="<?php echo get_field('appsPage_request_a_demo'); ?>" class="request_btn apply_btn"> 
                                Request a demo
                            </a>
                        </div>
                    </div>
                    

                </div>
            </div>
       </div>
       <div class="logoApps-wrap">
            <?php foreach($apps as $app): ?>
                <div class="app-logoBox">
                    <a href="<?php echo get_permalink($app->ID)?>" class="app-logo <?php the_field('app_name',$app->ID) ?>">
                        <div class="app-logo-img">            
                            <?php echo wp_get_attachment_image( get_field('app_logo',$app->ID),'medium',false) ?>
                            <div class="overlay">
                                <img src="<?php echo THEME_IMAGES; ?>arrow-right-apply4-white.png" alt="arrow right" title="arrow right">
                            </div>
                         </div> <?php // .app-logo-img ?>
                    </a>
                    <div class="desc-app-logoBox">
                        <p><?php the_field('app_short_description',$app->ID) ?></p>
                    </div>
                </div>
            <?php endforeach;?>
       </div>
    </div>
</div>

<?php get_footer(); ?>