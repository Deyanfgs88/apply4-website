<?php include_once 'includes/ysnp.php'; // this path needs to be added manually for each file ?>
<?php
/**
 * The template for displaying 404 pages (Not Found)
 **/
get_header(); 
?>

<div class="container-fluid">
    <div class="the-content">
        <div class="content404">
            <?php ses_custom_logo();?>
            <h1>404</h1>
            <h2>Page not found</h2>
            <p>Oops! Something has gone wrong – click <a href="<?php echo get_home_url(); ?>">here</a> to go back to the homepage.</p>
            
        </div>
    </div>
</div>