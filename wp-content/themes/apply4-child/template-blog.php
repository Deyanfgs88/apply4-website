<?php

/*

Template Name: Blog Page

*/

get_header(); 
the_post(); ?>

<div class="container-fluid">
   <div class="the-content">
        <div class="container-title-blog-filter-posts">
            <div class="title-blog">
                <h2><?php the_field('title_blog'); ?></h2>
            </div>
            <div class="container-template-filter-posts">
                <?php get_template_part('templates/partials/blog/filter-posts'); ?>
            </div>
        </div><!-- .container-title-blog-filter-posts -->

        <?php get_template_part('templates/partials/blog/list-posts'); ?>
        
        <?php
            global $a4_blog_query;
        ?>

        <div class="blog-pagination">
            <?php
                // Pagination
                $big = 999999999;

                echo paginate_links( array(
                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                    'format' => '?paged=%#%',
                    'current' => max( 1, get_query_var('paged') ),
                    'total' => $a4_blog_query->max_num_pages
                ) );
            ?>
        </div> <?php // .blog-pagination ?>

    </div> <!-- .the-content -->
</div> <!-- .container-fluid -->

<?php get_footer(); ?>


