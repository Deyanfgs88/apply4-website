<?php include 'ysnp.php';

add_action( 'customize_register', function ( $wp_customize ) {

    $wp_customize->add_setting( 
        'secundary_logo',
        array(
            'sanitize_callback' => 'ses_sanitize_upload',
        )
    );

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'secundary_logo', array(
        'label'    => __( 'Secundary Logo', THEME_SHORT_NAME ),
        'section'  => 'title_tagline',
        'settings' => 'secundary_logo',
    ) ) );

    function ses_sanitize_upload($input){
        return esc_url_raw($input); 
    }
});