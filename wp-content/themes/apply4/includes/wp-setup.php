<?php include 'ysnp.php'; // this path needs to be added manually for each file
    $include_files = array();


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  CONSTANTS
    // checking if this is a parent or a stand along theme
    if( ! defined( 'THEME_SHORT_NAME' ) ):
        define( 'THEME_SHORT_NAME' , 'wp'                                );
        define( 'THEME_LONG_NAME'  , 'wordpress-basic'                   );
        define( 'THEME_ROOT_PATH'  , get_template_directory() . '/'      );
        define( 'THEME_ROOT'       , get_template_directory_uri() .'/'   );
        define( 'PARENT_ROOT_PATH' , get_template_directory() . '/'      );
        define( 'PARENT_ROOT'      , get_stylesheet_directory_uri() .'/' );
    endif;

    define( 'PARENT_ASSETS'      , PARENT_ROOT . 'assets/'      );
    define( 'THEME_INCLUDES'    , 'includes/'                   );
    define( 'THEME_ADMIN'       , 'admin/'                      );
    define( 'THEME_ASSETS'      , THEME_ROOT       . 'assets/'  );
    define( 'THEME_ASSETS_PATH' , THEME_ROOT_PATH  . 'assets/'  );  

    include_once PARENT_ROOT_PATH . THEME_INCLUDES . 'wp-functions.php';

    ses_define_constants([
        'PARENT_FONTS'      => PARENT_ASSETS    . 'fonts/',
        'PARENT_TOOLS'      => PARENT_ASSETS    . 'tools/',
        'PARENT_JS'         => PARENT_ASSETS    . 'js/',
        'THEME_CSS'         => THEME_ASSETS     . 'css/',
        'THEME_JS'          => THEME_ASSETS     . 'js/',
        'THEME_JSON_PATH'   => THEME_ROOT_PATH  . 'assets/json/',
        'THEME_IMAGES'      => THEME_ASSETS     . 'images/',
        'THEME_IMAGES_PATH' => THEME_ASSETS_PATH . 'images/',
        'THEME_TOOLS'       => THEME_ASSETS     . 'tools/',
        'THEME_FONTS'       => THEME_ASSETS     . 'fonts/',
        'ADMIN_ROOT'        => THEME_ROOT       . 'admin/',
        'ADMIN_ROOT_PATH'   => THEME_ROOT_PATH  . 'admin/'       
    ],false);
    
    define( 'BOOTSTRAP_VERSION'   , '3.3.7' );
    define( 'FONTAWESOME_VERSION' , '4.7.0' );

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  FATHER FILES


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  CONSTANTS FOR CONFIGURATION
    define( 'CNF_USE_BOOTSTRAP' , true  );
    define( 'CNF_USE_SESSION'   , false );
    define( 'CNF_USE_AJAX'      , true  );
    define( 'CNF_USE_BS_MENU'   , true  );
    define( 'CNF_USE_SOCIALS'   , false );


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  CONSTANTS FOR SOCIAL MEDIAS
    if( true ):
        array_push( $include_files , 'social-media/social-class' );
        ses_define_constants([
            // Twitter
            'TW_CONSUMER_KEY'       => '',
            'TW_CONSUMER_SECRET'    => '',
            'TW_OAUTH_TOKEN'        => '',
            'TW_OAUTH_TOKEN_SECRET' => '',
            // LinkedIn
            'LK_CLIENT_ID'          => '', 
            'LK_CLIENT_SECRET'      => '', 
            'LK_REDIRECT_URI'       => ''  // the return url.
        ]);
    endif;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  LIST OF FILES TO BE LOADED THAT CAN BE OVERRIDDEN
    $include_files = array_merge( $include_files , array(
        'wp-debug-parent',
        'wp-custom-widgets-parent',
        'wp-customize-parent',
        'wp-enqueue-scripts-parent'
    ));

    if( true === CNF_USE_AJAX    ) array_push( $include_files , 'wp-ajax-requests' );
    if( true === CNF_USE_BS_MENU ) array_push( $include_files , 'wp-bootstrap-navwalker' );


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  FILES INCLUDED
    ses_include_files( PARENT_ROOT_PATH . THEME_INCLUDES , $include_files , false );