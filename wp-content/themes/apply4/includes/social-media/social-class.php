<?php include 'ysnp.php'; // this path needs to be added manually for each file
class ses_social{
    private $config;
    private $share;
    private $URLs;
    private $curl_opt;
    private $server_answer;
    function __construct( ){
        $this->share = array();
        $this->server_answer = array();
        $this->config = array(
            'twitter' => [
            'consumerKey'      => ses_get_constant( 'TW_CONSUMER_KEY'       ),
            'consumerSecret'   => ses_get_constant( 'TW_CONSUMER_SECRET'    ),
            'oauthToken'       => ses_get_constant( 'TW_OAUTH_TOKEN'        ),
            'oauthTokenSecret' => ses_get_constant( 'TW_OAUTH_TOKEN_SECRET' )
            ],
            'linkedin' => [
                'clientId'     => ses_get_constant( 'LK_CLIENT_ID'     ),
                'clientSecret' => ses_get_constant( 'LK_CLIENT_SECRET' )
            ]
        );
        $this->URLs = [
            'linkedin' => [
                'authorization' => 'https://www.linkedin.com/oauth/v2/authorization',
                'accessToken'   => 'https://www.linkedin.com/uas/oauth2/accessToken',
                'people'        => 'https://api.linkedin.com/v1/people/~',
                'data'          => 'https://api.linkedin.com/v1/people/~:(id,firstName,lastName,emailAddress)'
            ]
        ];

        $this->curl_opt = array(
            CURLOPT_POST            => true,
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_CONNECTTIMEOUT  => 10,
            CURLOPT_TIMEOUT         => 10,
        );

    }



/** 
* function get
*/
    function __get( $name ){
        $options = explode( '_' , $name );
        if( sizeof( $options ) > 1 && isset( $this->config[ $options[0] ][ $options[1] ] ) ):
            return  $this->config[ $options[0] ][ $options[1] ];  
        else:
            switch( $name ):
            case 'Twitter':
                if( empty( $this->share[ $name ] ) ) $this->share[ $name ] = $this->build_share( $name ); 
                return $this->share[ $name ];
            endswitch;
        endif;
        return null;
    }


    private function build_share( $name ){
        switch( $name ):
            case 'LinkedIn':

                return null;
            case 'Twitter':
                require_once( 'twitteroauth/twitteroauth.php' );
                return new TwitterOAuth(
                    $this->twitter_consumerKey, 
                    $this->twitter_consumerSecret, 
                    $this->twitter_oauthToken, 
                    $this->twitter_oauthTokenSecret
                );
        endswitch;
    }

    function linkify($value, $protocols = array('http', 'mail'), array $attributes = array()) {
        // Link attributes
        $attr = '';
        foreach ($attributes as $key => $val) {
            $attr = ' ' . $key . '="' . htmlentities($val) . '"';
        }
        
        $links = array();
        
        // Extract existing links and tags
        $value = preg_replace_callback('~(<a .*?>.*?</a>|<.*?>)~i', function ($match) use (&$links) { return '<' . array_push($links, $match[1]) . '>'; }, $value);
        
        // Extract text links for each protocol
        foreach ( (array)$protocols as $protocol) {
            switch ($protocol):
                case 'http':
                case 'https':
                    $value = preg_replace_callback('~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) { if ($match[1]) $protocol = $match[1]; $link = $match[2] ?: $match[3]; return '<' . array_push($links, "<a $attr href=\"$protocol://$link\">$link</a>") . '>'; }, $value); 
                    break;
                case 'mail':
                    $value = preg_replace_callback('~([^\s<]+?@[^\s<]+?\.[^\s<]+)(?<![\.,:])~', function ($match) use (&$links, $attr) { return '<' . array_push($links, "<a $attr href=\"mailto:{$match[1]}\">{$match[1]}</a>") . '>'; }, $value);
                    break;
                case 'twitter':
                    $value = preg_replace_callback('~(?<!\w)[@#](\w++)~', function ($match) use (&$links, $attr) { return '<' . array_push($links, "<a $attr href=\"https://twitter.com/" . ($match[0][0] == '@' ? '' : 'search/%23') . $match[1]  . "\">{$match[0]}</a>") . '>'; }, $value);
                    break;
                default:
                    $value = preg_replace_callback('~' . preg_quote($protocol, '~') . '://([^\s<]+?)(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) { return '<' . array_push($links, "<a $attr href=\"$protocol://{$match[1]}\">{$match[1]}</a>") . '>'; }, $value);
                    break;
            endswitch;
        }

        // Insert all link
        return preg_replace_callback('/<(\d+)>/', function ($match) use (&$links) { return $links[$match[1] - 1]; }, $value);
    }


    /**
    * addTweetEntityLinks
    *
    * adds a link around any entities in a twitter feed
    * twitter entities include urls, user mentions, and hashtags
    *
    * @author     Joe Sexton <joe@webtipblog.com>
    * @param      object $tweet a JSON tweet object v1.1 API
    * @return     string tweet
    */
    function addTweetEntityLinks( $tweet ) {
        // actual tweet as a string
        $tweetText = str_replace('…','...', $tweet->text );

        // create an array to hold urls
        $tweetEntites = array();

        // add each url to the array
        foreach( $tweet->entities->urls as $url ) {
            $tweetEntites[] = array (
                    'type'    => 'url',
                    'curText' => substr( $tweetText, $url->indices[0], ( $url->indices[1] - $url->indices[0] ) ),
                    'newText' => "<a href='".$url->expanded_url."' target='_blank'>".$url->display_url."</a>"
                );
        }  // end foreach

        // add each user mention to the array
        foreach ( $tweet->entities->user_mentions as $mention ) {
            $string = substr( $tweetText, $mention->indices[0], ( $mention->indices[1] - $mention->indices[0] ) );
            $tweetEntites[] = array (
                    'type'    => 'mention',
                    'curText' => substr( $tweetText, $mention->indices[0], ( $mention->indices[1] - $mention->indices[0] ) ),
                    'newText' => "<a href='http://twitter.com/".$mention->screen_name."' target='_blank'>".$string."</a>"
                );
        }  // end foreach

        // add each hashtag to the array
        foreach ( $tweet->entities->hashtags as $tag ) {
            $string = substr( $tweetText, $tag->indices[0], ( $tag->indices[1] - $tag->indices[0] ) );
            $tweetEntites[] = array (
                    'type'    => 'hashtag',
                    'curText' => substr( $tweetText, $tag->indices[0], ( $tag->indices[1] - $tag->indices[0] ) ),
                    'newText' => "<a href='http://twitter.com/search?q=%23".$tag->text."&src=hash' target='_blank'>".$string."</a>"
                );
        }  // end foreach

        // replace the old text with the new text for each entity
        foreach ( $tweetEntites as $entity ) {
            $tweetText = str_replace( $entity['curText'], $entity['newText'], $tweetText );
        } // end foreach

        return $tweetText;

    } // end addTweetEntityLinks()

    /*********************************************** TWITTER 
    * function get_twitter_posts
    */
    function get_twitter_posts( $amount_tweets = 5 ){
        return $this->Twitter->get('statuses/user_timeline', ['count' => $amount_tweets, 'exclude_replies' => true]);
    }


    function get_linkedin_button(){
        set_transient( 'linkedin_state' , ses_get_random_string( 10 ) , 60 );
        $data = [
            'response_type=code',
            'client_id='    . $this->config[ 'linkedin' ][ 'clientId' ],
            'redirect_uri=' . urlencode( ses_get_constant( 'LK_REDIRECT_URI' ) ),
            'state='        . get_transient( 'linkedin_state' ),
            'scope=r_emailaddress,r_basicprofile'
        ];
        $url = 'https://www.linkedin.com/oauth/v2/authorization?' . str_replace(' ' , '' , implode($data,'&') );
        echo $url . '<br>';
        echo '<a href="' . $url . '">Enlace</a>';
        
    }

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Function build_post_string */
    public function build_post_string( $social_media , $post_data = array() ){
        if( ! isset( $post_data ) || ! is_array( $post_data ) ) return '';
        switch( $social_media ):
            default: 
                $data = array();
                foreach( $post_data as $param => $value ):
                    $data[] = $param . '=' . $value;
                endforeach;
                return implode ( '&' , $data );

        endswitch;
    }

    public function linkedin_request_data(){

        $this->send_post_form( 'linkedin' , 'accessToken' , [
            'grant_type'    => 'authorization_code',
            'redirect_uri'  => urlencode( ses_get_constant( 'LK_REDIRECT_URI' ) ),
            'client_id'     => $this->config[ 'linkedin' ][ 'clientId' ],
            'client_secret' => $this->config[ 'linkedin' ][ 'clientSecret' ],
            'code'          => $_GET[ 'code' ],
        ]);

        $this->send_get_form( 'linkedin' , 'data' , [
            'format' =>'json',
            'oauth2_access_token' => $this->server_answer[ 'access_token' ]
        ]);
        return $this->server_answer;

    }


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * Function build_response_array */
    public function build_response_array( $social_media , $curl_output ){
        if ( ! isset( $curl_output ) ) return array();
        switch( $this->system_name ):
            default: 
                $this->server_answer = json_decode( $curl_output , true );

        endswitch;
        return $this->server_answer;
    }

    private function send_post_form( $social_media , $url_name , $post_data ){
        $curl_connection = curl_init();
        set_time_limit(0);
        $curl_opt = array_replace( $this->curl_opt , array(
            CURLOPT_URL             => $this->URLs[ $social_media ][ $url_name ],
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_MAXREDIRS       => 10,
            CURLOPT_TIMEOUT         => 30,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST   => "POST",
            CURLOPT_POSTFIELDS      => $this->build_post_string( $social_media , $post_data )
        ));

        foreach( $curl_opt as $opt => $value ):
            curl_setopt($curl_connection, $opt, $value);
        endforeach;

        $this->build_response_array( $social_media , curl_exec( $curl_connection ) );
        $this->curl_error_num = curl_errno( $curl_connection );
        //$this->curl_error_msg = (0 != curl_errno( $curl_connection ) )? curl_error( $curl_connection ): $this->get_payment_error();
        curl_close($curl_connection);
    }


    private function send_get_form( $social_media , $url_name , $get_data ){
        $curl_opt = array_replace( $this->curl_opt , array(
            CURLOPT_URL             => $this->URLs[ $social_media ][ $url_name ] . '?' . $this->build_post_string( $social_media , $get_data ),
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_MAXREDIRS       => 10,
            CURLOPT_TIMEOUT         => 30,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST   => "GET"
        ));
        //showText($curl_opt[CURLOPT_URL]);
        $curl_connection = curl_init();
        set_time_limit(0);
        foreach( $curl_opt as $opt => $value ):
            curl_setopt($curl_connection, $opt, $value);
        endforeach;

        $this->build_response_array( $social_media , curl_exec( $curl_connection ) );
        $this->curl_error_num = curl_errno( $curl_connection );
        //$this->curl_error_msg = (0 != curl_errno( $curl_connection ) )? curl_error( $curl_connection ): $this->get_payment_error();
        curl_close($curl_connection);
    }

}

add_action( 'template_redirect', function(){
    if( ! get_transient( 'linkedin_state' ) ) return false;
    showText('testing redirect');
    if( isset( $_GET[ 'state' ] ) && $_GET[ 'state' ] === get_transient( 'linkedin_state' ) ):
        $social = new ses_social();

        do_action( 'ses-after-linkedin-login' , $social->linkedin_request_data() );
    endif;
});