<?php include 'ysnp.php';

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ENQUEUE CSS
    add_action( 'wp_enqueue_scripts' , function(){

        wp_register_style( // - - - - - - - - - - - - - - - - Bootstrap
            THEME_SHORT_NAME . '-bootstrap',
            THEME_CSS . 'custom-bootstrap.css',
            array(),
            BOOTSTRAP_VERSION
        );

        wp_register_style( // - - - - - - - - - - - - - - - - Font Awesome
            THEME_SHORT_NAME . '-fontawe',
            PARENT_FONTS . 'font-awesome/' . FONTAWESOME_VERSION . '/css/font-awesome.min.css' ,
            array(),
            FONTAWESOME_VERSION
        );

        wp_enqueue_style( THEME_SHORT_NAME . '-bootstrap' );
        wp_enqueue_style( THEME_SHORT_NAME . '-fontawe' );

    }, 20 ); 



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ENQUEUE JS
    add_action( 'wp_enqueue_scripts' , function(){
        $deps = array('jquery'); 

        wp_register_script( // - - - - - - - - - - - - - - - Bootstrap
            THEME_SHORT_NAME . '-bootstrap',
            PARENT_TOOLS . 'bootstrap/' . BOOTSTRAP_VERSION . '/js/bootstrap.min.js',
            $deps,
            BOOTSTRAP_VERSION
        );

        $default_deps = array( 'jquery' );
        if( true === CNF_USE_AJAX ):
            wp_register_script( // - - - - - - - - - - - - - - - Ajax events
                THEME_SHORT_NAME . '-ajax-events',
                PARENT_JS .'ajax-events.js',
                array( 'jquery' ),
                '17.01.19' 
            );
            wp_localize_script( THEME_SHORT_NAME . '-ajax-events', 'ajax', array(
                'url'   => admin_url( 'admin-ajax.php' ) ,
                'auth'  => wp_create_nonce( THEME_LONG_NAME )
            ));
            array_push( $default_deps , THEME_SHORT_NAME . '-ajax-events' );
        endif;

        wp_register_script( // - - - - - - - - - - - - - - - Actions
            THEME_SHORT_NAME . '-actions',
            THEME_JS .'actions.js',
            $default_deps,
            '16.11.01' 
        );

        wp_enqueue_script( THEME_SHORT_NAME . '-bootstrap' );
        wp_enqueue_script( THEME_SHORT_NAME . '-actions' );

    }, 21 ); // END enqueue Javascript