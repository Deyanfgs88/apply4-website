<?php include 'ysnp.php';

// - - - - - - - - - - - - - - - - - - - - - - - - - - - Valid actions
    /**
    *   public => actions ONLY when not logged in
    *   users  => actions ONLY when logged in
    */
    global $valid_actions;
    $valid_actions = array(
        'public' => array(),
        'users'  => array()
    );

    foreach( $valid_actions[ 'public'  ] as $action ) add_action( 'wp_ajax_nopriv_' . $action , 'ses_ajax_handler' );
    unset( $action );

    foreach( $valid_actions[ 'users' ] as $action ) add_action( 'wp_ajax_' . $action        , 'ses_ajax_handler' );
    unset( $action );

// - - - - - - - - - - - - - - - - - - - - - - - - - - - ses_ajax_handler 
    function ses_ajax_handler(){
        // This should be improved
        if( true === CNF_USE_SESSION ) session_start();
        global $valid_actions;
        $response = $_POST ?: $_GET;
        if( !isset( $response ) ) return 'error'; // here we should handle this exception
        $action = $response[ 'action' ];

        if( ! in_array( $action , $valid_actions[ 'public' ] ) && ! in_array( $action , $valid_actions[ 'users' ] ) ) return 'error'; // another exception
        
        $data = $response[ 'query' ];

        $output = $action( $data );
        if( ! is_array( $output ) ) $output = array();
        wp_send_json( array_merge( array(
            'action' => $action
        ) , $output ) );
        wp_die();
    }
