<?php
include 'ysnp.php';
/**
*   How to name functions:
*       ses[_get]_{function_name}
*           ses     => secret source
*           [_get]  => if the function returns some value
*                      if the functions 'echo' something it should not use [_get]
*
*   All the functions should be sorted by name
*
*   All the [_get] functions should be separated from the none [_get]
*
*   The none [_get] functions should be an echo from the [_get] ones if possible
*
*   Use " // - - - - - - ses[_get]_{function_name} " before the functions
*
*   Use 2 blank spaces between functions
*
*   If the function is too long, add " // END ses[_get]_{function_name} " at the closing }
*/


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ses_go_home
function ses_go_home(){
    wp_redirect( get_home_url() );
    exit;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ses_show_url
function ses_show_url( $slug = null ){ echo get_url_by_slug( $slug ); }


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ses_singular_plural
function ses_singular_plural ($amount,$singular,$plural=null){
    echo get_singular_plural($amount,$singular,$plural);
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ses_the_custom_logo
function ses_custom_logo( $use_link = true ) {
    if ( function_exists( 'ses_get_custom_logo' ) ) {
        echo ses_get_custom_logo( $use_link );
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ses_define_constants
function ses_define_constants( $constants , $use_theme_name = true ){
    if( ! is_array( $constants ) ) return;

    $prefix = ( $use_theme_name ) ? THEME_SHORT_NAME . '_' : null;

    foreach( $constants as $key => $value )
        define( strtoupper( $prefix . $key ) , $value );
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ses_get_constant
function ses_get_constant( $constant = null ){
    if( ! isset( $constant )  || !defined(strtoupper( THEME_SHORT_NAME . '_' . $constant ))) return null;
    return constant( strtoupper( THEME_SHORT_NAME . '_' . $constant ) );
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ses_get_custom_logo
function ses_get_custom_logo( $use_link = true ) {
    if( $use_link ) return get_custom_logo();
    $custom_logo_id = get_theme_mod( 'custom_logo' );
    if( $custom_logo_id ):
        return wp_get_attachment_image( $custom_logo_id, 'full', false, array(
            'class'    => 'custom-logo',
            'itemprop' => 'logo'
        ) );
    endif;
}


/**
    * get json from file
    * @access public
    * @param {string}  $file_name    - name of the file without .json
    * @param {boolean} $output_array - When TRUE, returned objects will be converted into arrays
    * @param {boolean} $asscoc       - When TRUE, returned objects will be converted into associative arrays. Will be ignored if $output_array is set to false
    *
    * @example <caption> Usage of the function </caption>
    * ses_get_json_from_file('file_name);               => returns an associative array if not empty
    *
    * @return null or an empty array if the file_name is empty, the file does not exist or bad json format and array or string if success
*/
function ses_get_json_from_file( $file_name = null , $output_array = true, $assoc = true ){
    // checking file name is not empty
    if( ! isset( $file_name ) )
        return ( ( true === $output_array ) ? array() : null );

    // checking the file exists
    if( ! file_exists( THEME_JSON_PATH . (string)$file_name . '.json' ) ) 
        return ( ( true === $output_array ) ? array() : null );

    // reading json from file
    $json_output = file_get_contents( THEME_JSON_PATH . (string)$file_name . '.json' );
    if( false === $json_output )
        return ( ( true === $output_array ) ? array() : null );

    // trying to return data as array
    if ( true === $output_array ):
        $json_output = json_decode( $json_output , $assoc );
        if ( is_null( $json_output ) || false === $json_output ) 
            return array();
    endif;

    return $json_output;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ses_get_random_string
function ses_get_random_string( $length = 6 ){
    $str = '';
    $characters = array_merge( 
        range('A','Z') , 
        range('a','z') , 
        range('0','9') 
    );
    $max = count($characters) - 1;
    for ($i = 0; $i < $length; $i++) $str .= $characters[ mt_rand(0, $max) ];
    return $str;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ses_get_singular_plural
function ses_get_singular_plural( $amount , $singular , $plural=null ){
    if( ! $plural ) $plural = $singular . 's';
    return ( (int)$amount == 1 ) ? $singular : $plural;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ses_get_url_by_slug
function ses_get_url_by_slug( $slug = null ){
    if( ! $slug ) return '#';
    $path = get_page_by_path( $slug );
    if ( ! $path ) return '';
    return get_permalink( $path->ID );
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ses_set_constant
function ses_set_constant( $name , $value ){
    if( ! defined( $name ) ) define( $name , $value );
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ses_include_files
function ses_include_files( $folder_path , $files , $include_once = true ){
 
    if( ! is_array( $files ) ) return;

    foreach( $files as $file_name ):
        if( file_exists( $folder_path . $file_name . '.php' ) ):
            if(true === $include_once):
                include_once $folder_path . $file_name . '.php';
            else:
                include $folder_path . $file_name . '.php';
            endif;
        endif;
    endforeach;

}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ses_include_files
function ses_get_template_part( $folder_path , $files, $require_once = true ){
    
    if( ! is_array( $files ) ) return;

    foreach( $files as $file ):
        get_template_part( $folder_path , $file );
    endforeach;

}