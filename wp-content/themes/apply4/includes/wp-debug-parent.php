<?php
include 'ysnp.php';

// - - - - - - - - - - - - - - - - - - - - - - - - print_array
if( ! function_exists( 'print_array' ) ):
    function print_array( $array = array() ){
        if( true != WP_DEBUG ) return;
        echo '<pre>';
        print_r($array);
        echo '</pre>';
    }
endif;


// - - - - - - - - - - - - - - - - - - - - - - - - showArray
if( ! function_exists( 'showArray' ) ):
    function showArray( $array , $title = null){
        showTitle( $title );
        print_array( $array );
        track_me();
    }
endif;


// - - - - - - - - - - - - - - - - - - - - - - - - showArrayAndDie
if( ! function_exists( 'showArrayAndDie' ) ):
    function showArrayAndDie( $array , $title = null){
        showArray( $array , $title );
        wp_die();
    }
endif;


// - - - - - - - - - - - - - - - - - - - - - - - - showText
if( ! function_exists( 'showText' ) ):
    function showText( $text , $title = null ){
        showTitle( $title );
        echo '<p>' . $text . '</p>';
        track_me();
    }
endif;


// - - - - - - - - - - - - - - - - - - - - - - - - showTitle
if( ! function_exists( 'showTitle' ) ):
    function showTitle( $title = null ){
        if( true != WP_DEBUG ) return;
        if( $title ) echo '<h2>' . $title . '</h2>';
    }
endif;


// - - - - - - - - - - - - - - - - - - - - - - - - showTextAndDie
if( ! function_exists( 'showTextAndDie' ) ):
    function showTextAndDie( $text , $title = null ){
        showText( $text , $title );
        wp_die();
    }
endif;


// - - - - - - - - - - - - - - - - - - - - - - - - track_me
if( ! function_exists( 'track_me' ) ):
    function track_me( $die = false ){
        if( true != WP_DEBUG ) return;
        $path = debug_backtrace()[1];
        $path[ 'file' ] = substr( $path[ 'file' ] , strlen( ABSPATH . 'wp-content/') );
        echo 
        '<p>' .
            '<strong style="color: #008080">' . $path[ 'function' ] . '( )</strong>' .
            '<span style="color: #E9967A"> => </span>' . 
            '<strong>' . $path[ 'file' ] . ':</strong>' .
            '<span style="color: red">' . $path[ 'line' ] . '</span>' .
        '</p>';
        if( $die ) wp_die();
    }
endif;