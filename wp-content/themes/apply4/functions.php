<?php include_once 'includes/ysnp.php'; // this path needs to be added manually for each file

    if( ! defined( 'THEME_SHORT_NAME' ) ) include_once 'includes/wp-setup.php';

    $include = array(
        //'wp-enqueue-scripts' // it contains bootstrap in the backend. We should check this out more carefully. 
    );

    foreach( $include as $file ):
        if( ! file_exists( ADMIN_ROOT_PATH . THEME_INCLUDES . $file . '.php' ) ) continue;
        include_once  THEME_ADMIN . THEME_INCLUDES . $file . '.php';
    endforeach;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - HOOK: INIT
    add_action('init', function(){ 
        if( !session_id() ) session_start();
    });


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - HOOK: AFTER SETUP THEME
    add_action( 'after_setup_theme' , function(){
    // add_image_size       => Documentation: https://developer.wordpress.org/reference/functions/add_image_size/
        global $image_sizes;
        $image_sizes = array(
            //'theme-thumb' => array( 'width' => 300 , 'height' => 300 , 'crop' => true ) // this is an example
        );

        foreach( $image_sizes as $size_id => $image_size ):
            add_image_size( 
                $size_id,
                $image_size[ 'width' ],
                $image_size[ 'height' ],
                $image_size[ 'crop' ]
            );
        endforeach;

        if( ! empty( $image_sizes ) && is_array( $image_sizes ) ):
            // this adds the custom image sizes to the wordpress gallery
            add_filter( 'image_size_names_choose' , function( $sizes ){
                global $image_sizes;
                return array_merge( $sizes, $image_sizes );
            });
        endif;
        // cleaning memory
        $image_sizes = array();
        unset( $image_sizes );
    // add_theme_support    => Documentation: https://developer.wordpress.org/reference/functions/add_theme_support/
        add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form',  'caption' ) );
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'title-tag' ); 
        add_theme_support( 'custom-logo', array(
            'flex-height' => true,
            'flex-width'  => true,
            'height'      => 100,
            //'header-text' => array( 'site-title', 'site-description' ), // change the text if needed
            'width'       => 400
        ) );


    // register_nav_menu    => Documentation: https://codex.wordpress.org/Function_Reference/register_nav_menu
        register_nav_menu( 'primary' , 'main-menu' );


    // removing admin bar unless you are an administrator
        if( !current_user_can( 'administrator' ) && !is_admin( ) ):
            show_admin_bar(false);
            add_editor_style( );
        endif;

    } ); // END after_setup_theme


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - HOOK: WP HEAD
    add_action( 'wp_head' , function(){
        $head_lines = [
            '<meta charset="' . get_bloginfo( 'charset' ) . '">',
            '<meta name="viewport" content="width=device-width, initial-scale=1">'
        ];

        if( file_exists( THEME_ROOT_PATH . 'favicon.ico' ) )
            $head_lines[] = '<link rel="shortcut icon" href="' . THEME_ROOT . 'favicon.ico" />';
        elseif( file_exists( THEME_IMAGES_PATH . 'favicons/favicon-16.png' ) )
            $head_lines[] = '<link rel="shortcut icon" href="' . THEME_IMAGES . 'favicons/favicon-16.png" />';

        $file = null;
        foreach( array(16,76,120,152) as $icon_size ):
            $file = 'favicons/favicon-' . $icon_size . '.png';
            if( file_exists( THEME_IMAGES_PATH . $file ) )
                $head_lines[] = '<link rel="apple-touch-icon" sizes="' . $icon_size . 'x' . $icon_size . '" href="' . THEME_IMAGES . $file . '" />';
        endforeach;
        foreach( $head_lines as $line) echo $line . "\n";
    } , 5 );

// - - - - - - - - - - (why so many hyphens and spaces?) - - - - - - - -  DISABLE AUTHOR ARCHIVES
function shapeSpace_disable_author_archives() {
	
	if (is_author()) {
		
		global $wp_query;
		$wp_query->set_404();
		status_header(404);
		
	} else {
		
		redirect_canonical();
		
	}
	
}
remove_filter('template_redirect', 'redirect_canonical');
add_action('template_redirect', 'shapeSpace_disable_author_archives');
