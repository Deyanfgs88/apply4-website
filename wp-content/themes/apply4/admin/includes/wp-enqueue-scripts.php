<?php include 'ysnp.php';

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ENQUEUE CSS
    add_action ( 'admin_enqueue_scripts' , function(){
        
        wp_register_style( // - - - - - - - - - - - - - - - - Bootstrap
            THEME_SHORT_NAME . '-admin-bootstrap',
            THEME_TOOLS .'bootstrap/'. BOOTSTRAP_VERSION . '/css/bootstrap.min.css',
            array(),
            BOOTSTRAP_VERSION
        );
        wp_enqueue_style( THEME_SHORT_NAME . '-admin-bootstrap' );

    },21); // END enqueue CSS



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ENQUEUE JS
    add_action ( 'admin_enqueue_scripts' , function(){

        wp_register_script( // - - - - - - - - - - - - - - - Bootstrap
            THEME_SHORT_NAME . '-admin-bootstrap',
            THEME_TOOLS . 'bootstrap/' . BOOTSTRAP_VERSION . '/js/bootstrap-hack.js',
            array( 'jquery' ),
            '16.11.01' 
        );
        wp_enqueue_script( THEME_SHORT_NAME . '-admin-bootstrap' );


    },22); // END enqueue CSS
