<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */


define( 'RW_ENVIRONMENT' , 'development' );  // development, staging, production

$connection_data = array(
    // this is your pc or the local server you are using. And by "local server" I mean a pc in your local network. 
    'development' => array(
        'DB_NAME'     => '',
        'DB_USER'     => '',
        'DB_PASSWORD' => '',
        'DB_HOST'     => ''
    ),
    // this is for testing, usually {project_name}.launching-soon.com
    'staging' => array(
        'DB_NAME'     => '',
        'DB_USER'     => '',
        'DB_PASSWORD' => '',
        'DB_HOST'     => ''
    ),
    // this is the real site
    'production' => array(
        'DB_NAME'     => '',
        'DB_USER'     => '',
        'DB_PASSWORD' => '',
        'DB_HOST'     => ''
    )
);


foreach( $connection_data[ RW_ENVIRONMENT ] as $key => $value ) define( $key , $value );

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
define('FS_METHOD', 'direct');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'put your unique phrase here');
define('SECURE_AUTH_KEY',  'put your unique phrase here');
define('LOGGED_IN_KEY',    'put your unique phrase here');
define('NONCE_KEY',        'put your unique phrase here');
define('AUTH_SALT',        'put your unique phrase here');
define('SECURE_AUTH_SALT', 'put your unique phrase here');
define('LOGGED_IN_SALT',   'put your unique phrase here');
define('NONCE_SALT',       'put your unique phrase here');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ss_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
// let's debug shit, or not
define( 'WP_DEBUG', true );
// let's send the debug output to a log
define( 'WP_DEBUG_LOG', true );
// Let's NOT output debug messages to the screen
define( 'WP_DEBUG_DISPLAY', true );
// Let's not let wordpress admins modify files in plugins and themes
define( 'DISALLOW_FILE_EDIT', true );

/* https://codex.wordpress.org/Editing_wp-config.php#WordPress_address_.28URL.29 */
/* WP_HOME is the URL for your site (the bare domain) */
define( 'WP_HOME', 'http://' . $_SERVER[HTTP_HOST] .'/apply4' );

/* WP_SITEURL is where the wordpress files are installed */
define( 'WP_SITEURL', WP_HOME . '/wp' );

/* https://codex.wordpress.org/Editing_wp-config.php#Moving_wp-content_folder */
define( 'WP_CONTENT_DIR', dirname(__FILE__) . '/wp-content' );
define( 'WP_CONTENT_URL', WP_HOME . '/wp-content' );

/* Disable editing files via the admin so people don't overwrite our work */
define('DISALLOW_FILE_EDIT', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined( 'ABSPATH' ) )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
